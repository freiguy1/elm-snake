module Util exposing (..)

import Model exposing (..)


difficultyToMsDelay : Difficulty -> Float
difficultyToMsDelay difficulty =
    case difficulty of
        Easy ->
            170

        Medium ->
            135

        Hard ->
            100


difficultyToGrowSpeed : Difficulty -> Int
difficultyToGrowSpeed difficulty =
    case difficulty of
        Easy ->
            3

        Medium ->
            5

        Hard ->
            8


difficultyToFoodCount : Difficulty -> Int
difficultyToFoodCount difficulty =
    case difficulty of
        Easy ->
            3

        Medium ->
            2

        Hard ->
            1


oppositeDirections : Direction -> Direction -> Bool
oppositeDirections d1 d2 =
    if (d1 == Left || d2 == Left) && (d1 == Right || d2 == Right) then
        True

    else if (d1 == Up || d2 == Up) && (d1 == Down || d2 == Down) then
        True

    else
        False
