module Model exposing (..)

import Time exposing (Posix)


type Msg
    = Tick Posix
    | KeyDown String
    | NewFood Int Int
    | StartGame
    | DifficultyChanged Difficulty
    | GameBoardSizeChanged Int
    | ChangeDirection Direction
    | TogglePause


type Direction
    = Up
    | Down
    | Left
    | Right


type Difficulty
    = Easy
    | Medium
    | Hard


type CellState
    = Snake
    | Food
    | Empty


type GameState
    = Initializing
    | Playing
    | Paused
    | GameOver


type alias Options =
    { difficulty : Difficulty
    , gameBoardSize : Int
    }


type alias Point =
    { x : Int, y : Int }


type alias Model =
    { snake : List Point
    , currentDirection : Direction
    , nextDirections : List Direction -- last item in list is next direction
    , foods : List Point
    , gameState : GameState
    , options : Options
    , score : Int
    }


difficultyFromString : String -> Difficulty
difficultyFromString str =
    case String.toLower str of
        "easy" ->
            Easy

        "medium" ->
            Medium

        "hard" ->
            Hard

        _ ->
            Easy
