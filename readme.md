## Debugging

1. `elm make --debug --output=app.js Main.elm`
2. Open index-dev.html in a browser

You have to run that command every time you make a change. Then refresh your browser. This is the hit I take for not using a sophisticated builder like webpack or something, but the project remains very minimal.

## Formatting

You need the npm package elm-format installed globally, but then this does the dirty work.

`elm-format .`