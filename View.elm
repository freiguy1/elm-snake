module View exposing (view)

import Canvas exposing (Renderable, Shape, rect, shapes)
import Canvas.Settings exposing (..)
import Color
import Html exposing (..)
import Html.Attributes exposing (class, href, selected, style)
import Html.Events exposing (on, onClick)
import Json.Decode as Decode
import Model exposing (..)
import Util exposing (..)


view : Model -> Html Msg
view model =
    let
        wrapper =
            \body -> div [] [ headerView, body ]
    in
    case model.gameState of
        Initializing ->
            wrapper (initializingView model)

        Playing ->
            wrapper (gameView model)

        Paused ->
            wrapper (gameView model)

        GameOver ->
            wrapper (gameOverView model)


headerView : Html Msg
headerView =
    header []
        [ h1 [] [ text "Snake" ]
        , em []
            [ span [] [ text "classic snake written in " ]
            , a [ href "http://elm-lang.org/" ] [ text "elm" ]
            , span [] [ text " by " ]
            , a [ href "http://ethanfrei.com" ] [ text "ethan frei" ]
            , span [] [ text ". (" ]
            , a [ href "https://gitlab.com/freiguy1/elm-snake" ] [ text "source" ]
            , span [] [ text ", " ]
            , a [ href "http://ethanfrei.com/posts/snake-in-elm.html" ] [ text "blog post" ]
            , span [] [ text ")" ]
            ]
        ]



-- INITIALIZING VIEW


initializingView : Model -> Html Msg
initializingView model =
    div [ class "container" ]
        [ h2 [ class "options" ] [ text "Options" ]
        , newGameForm model
        ]


newGameForm : Model -> Html Msg
newGameForm model =
    div [ class "new-game-form" ]
        [ div []
            [ span [] [ text "Difficulty" ]
            , select [ onDifficultyChanged ]
                [ option [ selected (model.options.difficulty == Easy) ] [ text "Easy" ]
                , option [ selected (model.options.difficulty == Medium) ] [ text "Medium" ]
                , option [ selected (model.options.difficulty == Hard) ] [ text "Hard" ]
                ]
            ]
        , div []
            [ span [] [ text "Game Size" ]
            , select [ onGameBoardSizeChanged ]
                [ option
                    [ selected (model.options.gameBoardSize == 10) ]
                    [ text "10" ]
                , option
                    [ selected (model.options.gameBoardSize == 15) ]
                    [ text "15" ]
                , option
                    [ selected (model.options.gameBoardSize == 20) ]
                    [ text "20" ]
                , option
                    [ selected (model.options.gameBoardSize == 25) ]
                    [ text "25" ]
                ]
            ]
        , button [ onClick StartGame ] [ text "Start Game" ]
        ]


onDifficultyChanged : Html.Attribute Msg
onDifficultyChanged =
    let
        targetValueDecoder =
            Decode.at [ "target", "value" ] Decode.string

        stringToMsg =
            \s -> DifficultyChanged (difficultyFromString s)

        decoder =
            Decode.map stringToMsg targetValueDecoder
    in
    on "change" decoder


onGameBoardSizeChanged : Html.Attribute Msg
onGameBoardSizeChanged =
    let
        targetValueDecoder =
            Decode.at [ "target", "value" ] Decode.string

        stringToMsg =
            \s -> GameBoardSizeChanged (Maybe.withDefault 0 (String.toInt s))

        decoder =
            Decode.map stringToMsg targetValueDecoder
    in
    on "change" decoder



-- GAME OVER VIEW


gameOverView : Model -> Html Msg
gameOverView model =
    div [ class "container" ]
        [ div []
            [ h2 []
                [ span [] [ text "Game Over | Score : " ]
                , span [] [ model.score |> String.fromInt |> text ]
                ]
            ]
        , gameBoardView model
        , h3 [] [ text "Play again? " ]
        , newGameForm model

        -- , buttonsView
        ]



-- GAME VIEW


gameView : Model -> Html Msg
gameView model =
    div [ class "container" ]
        [ h2 []
            [ span [] [ text "Score : " ]
            , span [] [ model.score |> String.fromInt |> text ]
            ]
        , gameBoardView model
        , pauseNoteView
        , buttonsView
        ]


pauseNoteView : Html Msg
pauseNoteView =
    div [ class "pause-note" ]
        [ em [] [ text "Press space to pause/resume" ]
        ]


buttonsView : Html Msg
buttonsView =
    div [ class "arrow-buttons" ]
        [ button [ class "arrow-button", onClick (ChangeDirection Up) ] [ text "Up" ]
        , div []
            [ button [ class "arrow-button", onClick (ChangeDirection Left), style "margin-right" "55px;" ] [ text "Left" ]
            , button [ class "arrow-button", onClick (ChangeDirection Right) ] [ text "Right" ]
            ]
        , button [ class "arrow-button", onClick (ChangeDirection Down) ] [ text "Down" ]
        ]


gameBoardView : Model -> Html Msg
gameBoardView model =
    Canvas.toHtml ( gameSize.x, gameSize.y )
        []
        [ shapes
            [ fill (Color.rgb255 0x44 0x44 0x44) ]
            [ rect ( 0, 0 ) (toFloat gameSize.x) (toFloat gameSize.y) ]
        , snakeView model
        , foodView model
        ]


foodView : Model -> Renderable
foodView model =
    shapes [ fill (Color.rgb255 0xFF 0xBD 0xBD) ]
        (List.map (\seg -> pointToRect model seg) model.foods)


snakeView : Model -> Renderable
snakeView model =
    shapes [ fill (Color.rgb255 0xC9 0xC9 0xFF) ]
        (List.map (\seg -> pointToRect model seg) model.snake)


pointToRect : Model -> Point -> Shape
pointToRect model point =
    let
        cellWidth =
            toFloat gameSize.x / toFloat model.options.gameBoardSize

        cellHeight =
            toFloat gameSize.x / toFloat model.options.gameBoardSize

        pixelsX =
            toFloat point.x * cellWidth

        pixelsY =
            toFloat point.y * cellHeight
    in
    rect ( pixelsX, pixelsY ) cellWidth cellHeight


gameSize : Point
gameSize =
    Point 500 500
